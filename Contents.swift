import Foundation

class Box<T> {

    private var closure: ((T) -> Void)?

    var value: T {
        didSet {
            closure?(value)
        }
    }

    init(_ value: T) {
        self.value = value
    }

    func subscribe(_ closure: @escaping (T) -> Void) -> Box<T> {
        self.closure = closure
        return self
    }
}


var b = Box<Double>(27.5).subscribe { (newValue) in
    print("Got the new value = \(newValue)")
}

b.value = 9.0
b.value = 30.5
b.value = 29.7
b.value = 2000



